# Instrument

Return combined licor and qcl data. it connects to both the licor and the qcl, data
is updated at the same frequency as the qcl.

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `instrument` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:instrument, "~> 0.1.0"}
  ]
end
```

In the supervisor start:

```elixir
Instrument.Reader
```

To use write a GenStage consumer that subscribes ti the Instrument.Producer.

Instrument.Reader is responsble for reading the data and Instrument.Producer
is a GenStage broadcast producer to dispatch the data. The producer should be started
before the reader.

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/instrument](https://hexdocs.pm/instrument).

