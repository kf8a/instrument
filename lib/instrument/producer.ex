defmodule Instrument.Producer do
  @moduledoc """
  GenStage producer that broadcasts the data collected from the instrument
  """

  use GenStage

  def start_link(_) do
    GenStage.start_link(__MODULE__, :ok, name: __MODULE__)
  end

  def init(:ok) do
    state = %{data: [], demand: 0}
    {:producer, state, dispatcher: GenStage.BroadcastDispatcher}
  end

  def handle_info(_, state), do: {:noreply, [], state}

  def add(event), do: GenServer.cast(__MODULE__, {:add, event})

  def handle_cast({:add, event}, state), do: {:noreply, [event], state}

  def handle_demand(_, state), do: {:noreply, [], state}

end
