defmodule Instrument.Reader do
  @moduledoc """
  Read the instrument and send the data to the GenStage producer

  Subscribes to the QCL/ICOS and connects the Licor. When it
  receives a datum from the QCL it reads a data point from the
  Licor and emits an %Instrument{} record to the GenStage producer
  """

  use GenServer

  require Logger

  def start_link(debug \\ false ) do
    GenServer.start_link(__MODULE__, %{debug: debug, data: %Instrument{}, line: 0}, name: Instrument.Reader)
  end

  def init(state) do
    case state[:debug] do
      true ->
        Process.send_after(self(), :tick, 1_000)
      false ->
        Qcl.Reader.register(self())
    end
    {:ok, state}
  end

  def handle_info(:tick, state) do
    # line = LineReader.get_line(state[:line])
    # new_line = Integer.mod(state[:line] + 1, 305812)
    # new_state=Map.put(state, :line, new_line)
    #
    # [_, _, _, _, raw_co2, raw_n2o, raw_ch4, date | _] = String.split(line, ",")
    # {co2, _ } = Float.parse(raw_co2)
    # {n2o, _ } = Float.parse(raw_n2o)
    # {ch4, _ } = Float.parse(raw_ch4)
    # {:ok, datetime, _} = DateTime.from_iso8601(date <> "Z")
    # datum = %Instrument{datetime: datetime,
    #   ch4: ch4,
    #   n2o: n2o,
    #   co2: co2,
    #   h2o: random_number(),
    #   n2o_dry: random_number(),
    #   gas_temperature_c: random_number(),
    #   ambient_temperature_c: random_number(),
    #   air_flow: random_number(),
    #   d15n: random_number(),
    #   d15n_a: random_number(),
    #   d15n_b: random_number(),
    #   d18o: random_number(),
    #   nox: random_number()
    # }
    datum = %Instrument{datetime: DateTime.utc_now(),
      ch4: random_number(),
      n2o: random_number(),
      co2: random_number(),
      h2o: random_number(),
      n2o_dry: random_number(),
      gas_temperature_c: random_number(),
      ambient_temperature_c: random_number(),
      air_flow: random_number(),
      d15n: random_number(),
      d15n_a: random_number(),
      d15n_b: random_number(),
      d18o: random_number(),
      nox: random_number()
    }
    Instrument.Producer.add(datum)
    Process.send_after(self(), :tick, 1_000)
    {:noreply, state}
  end

  # The dry N2O is calculated per the manual as
  # Xdry=Xwet/[(1-(XH2O/1E6)]
  def handle_info(%Qcl{} = qcl, state) do
    # licor = Licor.Reader.current_value
    licor = %Licor{}
    air_flow= Airflow.Reader.current_value
    noise = AudioMonitor.Reader.current_value
    nox = Nox.Reader.current_value
    state = case licor do
      %Licor{} ->
        datum = %Instrument{datetime: qcl.datetime,
          ch4: qcl.ch4_ppm_dry,
          n2o: qcl.n2o_ppb_dry,
          co2: random_number(),
          # co2: licor.co2,
          h2o: qcl.h2o_ppm,
          n2o_dry: qcl.n2o_ppm_dry,
          gas_temperature_c: qcl.gas_temperature_c,
          ambient_temperature_c: qcl.ambient_temperature_c,
          air_flow: air_flow.volumetric_flow,
          noise: noise.value,
          # d15n: qcl.d15n,
          # d15n_a: qcl.d15n_a,
          # d15n_b: qcl.d15n_b,
          # d18o: qcl.d18o,
          nox: nox
        }
        Instrument.Producer.add(datum)
        Map.put(state, :data, datum)
      _ ->
        Logger.warn "unkown licor value #{inspect licor}"
        state
    end
    {:noreply, state}
  end

  def handle_info(msg, state) do
    Logger.info "Unexpected message: #{inspect msg}"
    {:noreply, state}
  end

  defp random_number() do
    :rand.uniform(1000) / 1000
  end
end

defmodule LineReader do
  def get_line(line) do
    File.stream!("data.csv")
    |> Stream.with_index
    |> Stream.filter(fn {_value, index} -> index == line end)
    |> Enum.at(0)
    |> print_line
  end
  defp print_line({value, _line_number}), do: String.trim(value)
  defp print_line(_), do: {:error, "Invalid Line"}
end

