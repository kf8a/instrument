defmodule Instrument.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  def start(_type, _args) do
    # List all child processes to be supervised
    IO.inspect Application.get_env(:instrument, :debug)
    children = case Application.get_env(:instrument, :debug) do
      true ->
          [
          Instrument.Producer,
          # Instrument.Consumer,
          # { Instrument.Reader, Application.get_env(:instrument, :debug)}
        ]
      _ ->
          [
          # {Licor.Reader, Application.get_env(:instrument, :licor_port_serial_number)},
          {Nox.Reader, %{serial_number: Application.get_env(:instrument, :nox_port_serial_number), address: 42 + 128}},
          {Qcl.Reader, %{serial_number: Application.get_env(:instrument, :qcl_port_serial_number), parser: Qcl.Parser}},
          {Airflow.Reader, Application.get_env(:instrument, :airflow_port_serial_number)},
          {AudioMonitor.Reader, Application.get_env(:instrument, :noise_port_serial_number)},
          Instrument.Producer,
       #  Instrument.Consumer,
        ]
    end

    IO.puts "children"
    IO.inspect children

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: Instrument.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
