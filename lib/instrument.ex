defmodule Instrument do
  @moduledoc """
  Documentation for Instrument.

  TODO: minutes is here for the micos, It will be removed once I can refactor
  """
  defstruct datetime: DateTime.utc_now, co2: 0.0, n2o: 0.0, ch4: 0.0, h2o: 0.0, seconds: 0.0,
    ambient_temperature_c: 0.0, gas_temperature_c: 0.0, n2o_dry: 0.0, air_flow: 0.0, noise: 0.0,
    d15n: 0, d15n_a: 0, d15n_b: 0, d18o: 0, nox: 0, minute: 0.0

  @typedoc """
  A custom type that holds instrument data
  """
  @type t :: %Instrument{datetime: DateTime.t, co2: Float, n2o: Float, ch4: Float,
    h2o: Float, seconds: Float, ambient_temperature_c: Float, gas_temperature_c: Float,
    n2o_dry: Float, air_flow: Float, noise: Float, nox: Float, minute: Float}
end
