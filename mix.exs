defmodule Instrument.MixProject do
  use Mix.Project

  def project do
    [
      app: :instrument,
      version: "0.1.0",
      elixir: "~> 1.8",
      start_permanent: Mix.env() == :prod,
      deps: deps()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Instrument.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:gen_stage, "~> 1.0"},
      {:licor, github: "kf8a/licor"},
      {:nox, github: "kf8a/nox"},
      {:qcl, git: "git@gitlab.com:kf8a/qcl.git"},
      {:airflow, github: "kf8a/alicat", branch: "main"},
      {:audio_monitor, git: "git@gitlab.com:kf8a/audio_monitor.git"},
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: [:dev], runtime: false}
    ]
  end
end
